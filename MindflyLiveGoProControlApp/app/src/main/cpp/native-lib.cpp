#ifndef NATIVELIB_CPP
#define NATIVELIB_CPP
#include <jni.h>
#include <string>

extern "C" JNIEXPORT jstring JNICALL
Java_com_oversight_mindflylivegoprocontrolapp_MainActivity_stringFromJNI(
        JNIEnv* env,
        jobject /* this */) {
    std::string hello = "Hello from C++ test";
    return env->NewStringUTF(hello.c_str());
}
#endif