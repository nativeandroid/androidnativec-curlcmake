//
// Created by eitan on 01/10/2022.
//
#ifndef TESTINGCPP
#define TESTINGCPP
#include <jni.h>
#include <string>
#include <android/log.h>
#include "media.cpp"
#define  LOG_TAG    "GO-PRO-APP"
#define LOGD(...)  __android_log_print(ANDROID_LOG_DEBUG,LOG_TAG,__VA_ARGS__)

extern "C"
JNIEXPORT void JNICALL
Java_com_oversight_mindflylivegoprocontrolapp_MainActivity_testingJNI(JNIEnv *env, jobject /* this */) {
    //printf("testing here");
    //__android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, "Testing here");
    int result;

    result = system("pwd");

    LOGD("result is cp %d", result);
    char data[] = "Test";
    char data2[] = "Test2";
    char* c[] = {data, data2};
    int ret = media::runMain(2, c);
    LOGD("ret %d", ret);
}
#endif