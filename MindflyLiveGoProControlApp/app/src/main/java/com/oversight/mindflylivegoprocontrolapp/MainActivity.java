package com.oversight.mindflylivegoprocontrolapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import com.oversight.mindflylivegoprocontrolapp.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

    // Used to load the 'mindflylivegoprocontrolapp' library on application startup.
    static {
        System.loadLibrary("mindflylivegoprocontrolapp");
    }

    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        // Example of a call to a native method
        TextView tv = binding.sampleText;
        tv.setText(stringFromJNI());
        testingJNI();
    }

    /**
     * A native method that is implemented by the 'mindflylivegoprocontrolapp' native library,
     * which is packaged with this application.
     */
    public native String stringFromJNI();

    public native void testingJNI();
}